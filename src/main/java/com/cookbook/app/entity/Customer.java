package com.cookbook.app.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "customer"
        ,uniqueConstraints = @UniqueConstraint(
                name = "uc_customer_email",
                columnNames = "email"
        )
)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customer_seq")
    @SequenceGenerator(name = "customer_seq", sequenceName = "customer_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "firstname" , columnDefinition = "character varying(25)" , nullable = false)
    private String  firstName;

    @Column(name = "lastname" , columnDefinition = "character varying(25)")
    private String  lastName;

    @Column(name = "email", nullable = false, /*unique = true,*/ columnDefinition = "character varying(25)")
    private String email;

    @Column(name = "dob" , columnDefinition = "date" , nullable = false)
    private LocalDate dob;

    @Column(name = "mobile_number", nullable = false, unique = true, columnDefinition = "character varying(10)")
    private String mobileNumber;

    @Column(name = "sex" , columnDefinition = "character varying(6)")
    private String  sex;

    @Column(name = "ethnicity" , columnDefinition = "character varying(25)")
    private String ethnicity;

    @Column(name = "kitchen_details" , columnDefinition = "character varying(255)" )
    private String  kitchenDetails;

    @Column(name = "expectations_from_chef" , columnDefinition = "character varying(255)" )
    private String  expectationsFromChef;

    @Column(name = "address" , columnDefinition = "character varying(255)" ,nullable = false)
    private String  address;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Order> orders = new LinkedHashSet<>();

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }
}
