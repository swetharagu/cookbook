package com.cookbook.app.entity;

import com.cookbook.app.enumConstant.Ethnicity;
import com.cookbook.app.enumConstant.Sex;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "chef"
        ,uniqueConstraints = @UniqueConstraint(
                name = "uc_chef_email",
                columnNames = "email"
        )
)
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
//@NoArgsConstructor
@Builder
public class Chef {
        @javax.persistence.Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "chef_seq")
        @SequenceGenerator(name = "chef_seq", sequenceName = "chef_seq", allocationSize = 1)
        @Column(name = "id", nullable = false)
        private Long id;

        @Column(name = "firstname" , nullable = false , columnDefinition = "character varying(25)" )
        private String  firstName;

        @Column(name = "lastname" , columnDefinition = "character varying(25)" )
        private String  lastName;

        @Column(name = "email", nullable = false, unique = true, columnDefinition = "character varying(25)")
        private String email;

        @Column(name = "dob" , columnDefinition = "date" , nullable = false)
        private LocalDate dob;

        @Column(name = "mobile_number", nullable = false, unique = true, columnDefinition = "character varying(10)")
        private String mobileNumber;

        @Column(name = "height" , columnDefinition = "smallint" )
        private int height;

        @Column(name = "sex" , columnDefinition = "character varying(6)"  , nullable = false)
        @Enumerated(EnumType.STRING)
        private Sex sex;

        @Column(name = "ethnicity" , columnDefinition = "character varying(25)" , nullable = false)
        @Enumerated(EnumType.STRING)
        private Ethnicity ethnicity;

        @Transient
        private int age;

        @Column(name = "description" , columnDefinition = "character varying(255)" )
        private String  description;

        @Column(name = "address" , columnDefinition = "character varying(255)" ,nullable = false)
        private String  address;

        @OneToMany(mappedBy = "chef", cascade = CascadeType.ALL, orphanRemoval = true)
        @ToString.Exclude
        private Set<Order> orders = new LinkedHashSet<>();

        public void setAge(int age) {
                this.age = age;
        }

        public Integer getAge() {
                return Period.between(dob,LocalDate.now()).getYears();
        }

        public Set<Order> getOrders() {
                return orders;
        }

        public void setOrders(Set<Order> orders) {
                this.orders = orders;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
                Chef chef = (Chef) o;
                return id != null && Objects.equals(id, chef.id);
        }

        @Override
        public int hashCode() {
                return getClass().hashCode();
        }
}