package com.cookbook.app.api;

import com.cookbook.app.apiException.DuplicateDataException;
import com.cookbook.app.apiException.ItemNotFoundException;
import com.cookbook.app.entity.SpecialDish;
import com.cookbook.app.service.SpecialDishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("specialDish")
public class SpecialDishController implements SpecialDishService{

    @Autowired
    private SpecialDishService specialDishService;

    @GetMapping
    public List<SpecialDish> getAllSpecialDishes() {
        return specialDishService.getAllSpecialDishes();
    }

    @GetMapping("/name/{name}")
    public SpecialDish getSpecialDishByName(@PathVariable("name") String name) throws ItemNotFoundException {
        return specialDishService.getSpecialDishByName(name);
    }

    @PostMapping
    public void saveSpecialDish(SpecialDish specialDish) throws DuplicateDataException {
        specialDishService.saveSpecialDish(specialDish);
    }

    @PutMapping("/name")
    public void updateSpecialDishName(@RequestParam(value = "name") String name , @RequestParam(value = "updatedName") String updatedName) throws DuplicateDataException {
        specialDishService.updateSpecialDishName(name , updatedName);
    }

    @PutMapping("/description")
    public void updateSpecialDishDescription(@RequestParam(value = "name") String name, @RequestParam(value = "updatedDescription") String updatedDescription) {
        specialDishService.updateSpecialDishDescription(name , updatedDescription);
    }

    @PutMapping("/chefId")
    public void updateSpecialDishChefId(@RequestParam(value = "name") String name, @RequestParam(value = "chefId") String chefId) {
        specialDishService.updateSpecialDishChefId(name , chefId);
    }

    @DeleteMapping
    public void deleteByName(@RequestParam(value = "name") String name) {
        specialDishService.deleteByName(name);
    }
}

