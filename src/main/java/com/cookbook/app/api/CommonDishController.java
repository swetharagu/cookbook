package com.cookbook.app.api;

import com.cookbook.app.apiException.DuplicateDataException;
import com.cookbook.app.apiException.ItemNotFoundException;
import com.cookbook.app.entity.CommonDish;
import com.cookbook.app.service.CommonDishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("commonDish")
public class CommonDishController {

    @Autowired
    private CommonDishService commonDishService;

    @GetMapping
    public List<CommonDish> getAllCommonDishes() {
        return commonDishService.getAllCommonDishes();
    }

    @GetMapping("/name/{name}")
    public CommonDish getCommonDishByName(@PathVariable("name") String name) throws ItemNotFoundException {
        return commonDishService.getCommonDishByName(name);
    }

    @PostMapping
    public void saveCommonDish(CommonDish commonDish) throws DuplicateDataException {
        commonDishService.saveCommonDish(commonDish);
    }

    @PutMapping("/name")
    public void updateCommonDishName(@RequestParam(value = "name") String name , @RequestParam(value = "updatedName") String updatedName) throws DuplicateDataException {
        commonDishService.updateCommonDishName(name , updatedName);
    }

    @PutMapping("/description")
    public void updateCommonDishDescription(@RequestParam(value = "name") String name , @RequestParam(value = "updatedDescription") String updatedDescription) {
        commonDishService.updateCommonDishDescription(name , updatedDescription);
    }

    @DeleteMapping
    public void deleteByName(@RequestParam(value = "name") String name) {
        commonDishService.deleteByName(name);
    }
}
