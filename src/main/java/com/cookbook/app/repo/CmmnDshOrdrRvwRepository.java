package com.cookbook.app.repo;

import com.cookbook.app.entity.CmmnDshOrdrRvw;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CmmnDshOrdrRvwRepository extends JpaRepository<CmmnDshOrdrRvw, Long> {
//    List<CmmnDshOrdrRvw> findByCommonDishId(Long commonDishId);
    List<CmmnDshOrdrRvw> findByOrderId(Long orderId);
    List<CmmnDshOrdrRvw> findByReviewId(Long reviewId);
    List<CmmnDshOrdrRvw> findByOrderIdAndCommonDishId(Long orderId , Long commonDishId);
}