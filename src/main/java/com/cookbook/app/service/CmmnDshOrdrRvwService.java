package com.cookbook.app.service;

import com.cookbook.app.entity.CmmnDshOrdrRvw;

import java.util.List;

public interface CmmnDshOrdrRvwService {
    List<CmmnDshOrdrRvw> getAllCmmnDshOrdrRvws();
    List<CmmnDshOrdrRvw> getCmmnDshOrdrRvwByReviewId(Long reviewId);
    List<CmmnDshOrdrRvw> getCmmnDshOrdrRvwByOrderId(Long orderId);
    List<CmmnDshOrdrRvw> getCmmnDshOrdrRvwByOrderIdAndCommonDishId(Long orderId , Long commonDishId);
}
