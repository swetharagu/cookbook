package com.cookbook.app.service;

import com.cookbook.app.apiException.DuplicateDataException;
import com.cookbook.app.apiException.ItemNotFoundException;
import com.cookbook.app.entity.CommonDish;
import com.cookbook.app.repo.CommonDishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommonDishServiceImpl implements CommonDishService {

    @Autowired
    private CommonDishRepository commonDishRepository;

    @Override
    public List<CommonDish> getAllCommonDishes() {
        return commonDishRepository.findAll();
    }

    @Override
    public CommonDish getCommonDishByName(String name) throws ItemNotFoundException {
        return commonDishRepository.findByName(name)
                .orElseThrow(() -> new ItemNotFoundException("Common dish with name : " + name + " is not present"));
    }

    @Override
    public void saveCommonDish(CommonDish commonDish) throws DuplicateDataException {
        try{commonDishRepository.save(commonDish);}
        catch(Exception exception){
            throw new DuplicateDataException("Common dish with name : " + commonDish.getName() + " already exists");
        }
    }

    @Override
    public void updateCommonDishName(String name, String updatedName) throws DuplicateDataException {
        try{commonDishRepository.updateNameByName(name , updatedName);}
        catch (Exception exception){
            throw new DuplicateDataException("Common dish name : " + updatedName + " already exists. Try giving another name");
        }
    }

    @Override
    public void updateCommonDishDescription(String name, String updatedDescription) {
        commonDishRepository.updateDescriptionByName(name , updatedDescription);
    }

    @Override
    public void deleteByName(String name) {
        commonDishRepository.deleteByName(name);
    }
}