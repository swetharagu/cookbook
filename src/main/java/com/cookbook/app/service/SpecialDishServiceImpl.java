package com.cookbook.app.service;

import com.cookbook.app.apiException.DuplicateDataException;
import com.cookbook.app.apiException.ItemNotFoundException;
import com.cookbook.app.entity.SpecialDish;
import com.cookbook.app.repo.SpecialDishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SpecialDishServiceImpl implements SpecialDishService {

    @Autowired
    private SpecialDishRepository specialDishRepository;

    @Override
    public List<SpecialDish> getAllSpecialDishes() {
        return specialDishRepository.findAll();
    }

    @Override
    public SpecialDish getSpecialDishByName(String name) throws ItemNotFoundException {
        return specialDishRepository.findByName(name)
                .orElseThrow(() -> new ItemNotFoundException("Special dish with name : " + name + " is not present"));
    }

    @Override
    public void saveSpecialDish(SpecialDish specialDish) throws DuplicateDataException {
        try{specialDishRepository.save(specialDish);}
        catch(Exception exception){
            throw new DuplicateDataException("Special dish with name : " + specialDish.getName() + " already exists");
        }
    }

    @Override
    public void updateSpecialDishName(String name, String updatedName) throws DuplicateDataException {
        try{specialDishRepository.updateNameByName(name , updatedName);}
        catch (Exception exception){
            throw new DuplicateDataException("Common dish name : " + updatedName + " is taken by some other Chef. Try giving another name");
        }
    }

    @Override
    public void updateSpecialDishDescription(String name, String updatedDescription) {
        specialDishRepository.updateDescriptionByName(name , updatedDescription);
    }

    @Override
    public void updateSpecialDishChefId(String name, String chefId) {
        specialDishRepository.updateChefIdByName(name , chefId);
    }

    @Override
    public void deleteByName(String name) {
        specialDishRepository.deleteByName(name);
    }    
}
