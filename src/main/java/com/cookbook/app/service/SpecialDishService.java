package com.cookbook.app.service;

import com.cookbook.app.apiException.DuplicateDataException;
import com.cookbook.app.apiException.ItemNotFoundException;
import com.cookbook.app.entity.SpecialDish;

import java.util.List;

public interface SpecialDishService {
    List<SpecialDish> getAllSpecialDishes();
    SpecialDish getSpecialDishByName(String name) throws ItemNotFoundException;
    void saveSpecialDish(SpecialDish specialDish) throws DuplicateDataException;
    void updateSpecialDishName(String name , String updatedName) throws DuplicateDataException;
    void updateSpecialDishDescription(String name , String updatedDescription);
    void updateSpecialDishChefId(String name , String chefId);
    void deleteByName(String name);
}
