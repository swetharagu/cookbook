package com.cookbook.app.service;

import com.cookbook.app.apiException.DuplicateDataException;
import com.cookbook.app.apiException.ItemNotFoundException;
import com.cookbook.app.entity.Chef;
import com.cookbook.app.enumConstant.Ethnicity;
import com.cookbook.app.enumConstant.Sex;
import com.cookbook.app.repo.ChefRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//import static com.cookbook.app.service.validator.ChefValidator.*;

@Service
public class ChefServiceImpl implements ChefService{

    @Autowired
    private ChefRepository chefRepository;

    @Override
    public List<Chef> getAllChefs() {
        return chefRepository.findAll();
    }

    @Override
    public Chef getChefByEmail(String email) throws ItemNotFoundException {
        return chefRepository.findByEmail(email)
                .orElseThrow(() -> new ItemNotFoundException("Chef with email : " + email + " is not present"));
    }

    @Override
    public Chef getChefByMobileNumber(String mobileNumber) throws ItemNotFoundException {
        return chefRepository.findByMobileNumber(mobileNumber)
                .orElseThrow(() -> new ItemNotFoundException("Chef with mobile number : " + mobileNumber + " is not present"));
    }

//    private static List<String> validate(Chef chef){
//        return isMobileNumberValid()
//                .and(isEmailValid())
//                .and(isFirstNameValid())
//                .apply(chef , new ArrayList<>());
//    }

    private List<String> validate(Chef chef) {
//        Chef chefByMobileNumber = chefRepository.findByMobileNumber(chef.getMobileNumber()).orElse(null);
//        if(chefByMobileNumber == null){
//            Chef chefByEmailId = chefRepository.findByEmail(chef.getEmail()).orElse(null);
//        }
        return null;
    }

    //    @Override
//    public void saveChef(Chef chef) throws DuplicateDataException {
//        List<String> invalidDataList = validate(chef);
//        if(invalidDataList!=null && !invalidDataList.isEmpty()) throw new DuplicateDataException("Invalid data : " + invalidDataList.toString());
//        chefRepository.save(chef);
//    }
    @Override
    public void saveChef(Chef chef) throws DuplicateDataException {
        try{chefRepository.save(chef);}
        catch (Exception exception){
            StringBuffer message = new  StringBuffer("Chef with");
            chefRepository.findByEmail(chef.getEmail())
                    .ifPresent(chef1 -> message.append(" email:" + chef1.getEmail()));

            chefRepository.findByMobileNumber(chef.getMobileNumber())
                    .ifPresent(chef2 -> {
                        if(!message.toString().equals("Chef with")) message.append(" and");
                        message.append(" mobile Number:" + chef2.getMobileNumber());
                    });
            message.append(" already exists");
            throw new DuplicateDataException(message.toString());
        };
    }

    @Override
    public void updateChefEmail(String email , String updatedEmail) throws DuplicateDataException {
        try{chefRepository.updateEmailByEmail(email , updatedEmail);}
        catch (Exception exception){
            throw new DuplicateDataException("Email : " + updatedEmail + " is taken by some other Chef. Try giving another email");
        }
    }

    @Override
    public void updateChefMobileNumber(String email, String updatedNumber) throws DuplicateDataException {
//        var chef = chefRepository.findByMobileNumber(updatedNumber);
//        if(chef.isPresent()) throw new DuplicateDataException("Mobile number : " + updatedNumber + "is taken by some other Chef. Try giving another email");
        try{chefRepository.updateNumberByEmail(email , updatedNumber);}
        catch (Exception exception){
            throw new DuplicateDataException("Mobile number : " + updatedNumber + " is taken by some other Chef. Try giving another number");
        }
    }

    @Override
    public void updateChefAddress(String email, String updatedAddress) {
        chefRepository.updateChefAddress(email , updatedAddress);
    }

    @Override
    public void updateChefDescription(String email, String updatedDescription) {
        chefRepository.updateChefDescription(email , updatedDescription);
    }

    @Override
    public void updateChefEthnicity(String email, Ethnicity updatedEthnicity) {
        chefRepository.updateChefEthnicity(email , updatedEthnicity.toString());
    }

    @Override
    public void updateChefHeight(String email, String updatedHeight) {
        chefRepository.updateChefHeight(email , updatedHeight);
    }

    @Override
    public void updateChefFirstName(String email, String updatedFirstname) {
        chefRepository.updateChefFirstName(email , updatedFirstname);
    }

    @Override
    public void updateChefLastName(String email, String updatedLastname) {
        chefRepository.updateChefLastName(email , updatedLastname);
    }

    @Override
    public void updateChefSex(String email, Sex updatedSex) {
        chefRepository.updateChefSex(email , updatedSex.toString());
    }

    @Override
    public void deleteByEmail(String email) {
        chefRepository.deleteByEmail(email);
    }

    @Override
    public void deleteByMobileNumber(String number) {
        chefRepository.deleteByMobileNumber(number);
    }
}