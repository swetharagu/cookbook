package com.cookbook.app.service;

import com.cookbook.app.apiException.ItemNotFoundException;
import com.cookbook.app.entity.Review;
import com.cookbook.app.enumConstant.Star;
import com.cookbook.app.repo.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    private ReviewRepository reviewRepository;

    @Override
    public List<Review> getAllReviews() {
        return reviewRepository.findAll();
    }

    @Override
    public Review getReviewById(Long id) throws ItemNotFoundException {
        return reviewRepository.findById(id)
                .orElseThrow(() -> new ItemNotFoundException("Review with id : " + id + " is not present"));
    }

    @Override
    public void saveReview(Review review) {
        reviewRepository.save(review);
    }

    @Override
    public void updateReviewDescription(Long id, String updatedDescription) {
        reviewRepository.updateDescriptionById(id , updatedDescription);
    }

    @Override
    public void updateReviewStar(Long id, Star star) {
        reviewRepository.updateStarById(id , star.toString());
    }

    @Override
    public void deleteById(Long id) {
        reviewRepository.deleteById(id);
    }
}
