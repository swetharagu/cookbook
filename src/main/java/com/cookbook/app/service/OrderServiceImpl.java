package com.cookbook.app.service;

import com.cookbook.app.entity.Order;
import com.cookbook.app.enumConstant.OrderStatus;
import com.cookbook.app.repo.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    @Override
    public List<Order> getOrderByCustomerId(Long customerId) {
        return orderRepository.findByCustomerId(customerId);
    }

    @Override
    public List<Order> getOrderByChefId(Long chefId) {
        return orderRepository.findByChefId(chefId);
    }

    @Override
    public List<Order> getOrderByChefIdAndCustomerId(Long chefId, Long customerId) {
        return orderRepository.findByChefIdAndCustomerId(chefId , customerId);
    }

    @Override
    public List<Order> getOrderByOrderStatus(OrderStatus orderStatus) {
        return orderRepository.findByOrderStatus(orderStatus.toString());
    }

    @Override
    public List<Order> getOrderByChefIdAndStatus(Long chefId, OrderStatus orderStatus) {
        return orderRepository.findByChefIdAndOrderStatus(chefId , orderStatus.toString());
    }

    @Override
    public List<Order> getOrderByCustomerIdAndStatus(Long customerId, OrderStatus orderStatus) {
        return orderRepository.findByCustomerIdAndOrderStatus(customerId , orderStatus.toString());
    }

    @Override
    public List<Order> getOrderByChefIdCustomerIdAndStatus(Long chefId, Long customerId, OrderStatus orderStatus) {
        return orderRepository.findByChefIdAndCustomerIdAndOrderStatus(chefId , customerId , orderStatus.toString());
    }

    @Override
    public List<Order> getOrderBetween(Timestamp from, Timestamp to) {
        return orderRepository.findByTimeOfOrderBetween(from , to);
    }

    @Override
    public List<Order> getOrderByChefIdBetween(Long chefId, Timestamp from, Timestamp to) {
        return orderRepository.findByChefIdAndTimeOfOrderBetween(chefId , from , to);
    }

    @Override
    public List<Order> getOrderByCustomerIdBetween(Long customerId, Timestamp from, Timestamp to) {
        return orderRepository.findByCustomerIdAndTimeOfOrderBetween(customerId , from , to);
    }

    @Override
    public List<Order> getOrderByChefIdAndCustomerIdBetween(Long chefId, Long customerId, Timestamp from, Timestamp to) {
        return orderRepository.findByChefIdAndCustomerIdAndTimeOfOrderBetween(chefId , customerId , from , to);
    }

    @Override
    public void updateOrderDescription(Long id, String updatedDescription) {
        orderRepository.updateOrderDescription(id , updatedDescription);
    }

    @Override
    public void updateOrderOrderStatus(Long id, String updatedOrderStatus) {
        orderRepository.updateOrderOrderStatus(id , updatedOrderStatus);
    }
}
