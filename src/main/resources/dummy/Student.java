package com.cookbook.app.dummy;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;

//@Entity
@Table(name = "tbl_student",
       uniqueConstraints = @UniqueConstraint(
               name = "emailid_unique",
               columnNames = "email_address"
       )
)
@Data
@AllArgsConstructor
//@NoArgsConstructor
@Builder
public class Student {
    @Id
    @SequenceGenerator(name = "student_sequence", sequenceName = "student_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "student_sequence")
    @Column(name = "id", nullable = false)
    private Long id;

    private String name;

    @Column(name = "email_address",
            nullable = false)
    private String emailId;

    private LocalDate dob;

    @Transient
    private Integer age;

    @Embedded
    private Guardian guardian;

    public Student(Long id, String name, String email, LocalDate dob/*, Integer age*/) {
        this.id = id;
        this.name = name;
        this.emailId = email;
        this.dob = dob;
        System.out.println("qqqqqqqqqq");
       // this.age = age;
    }

    public Student() {
        System.out.println("eeeeeeeeeeeeee");
    }

    public Student(String name, String email, LocalDate dob/*, Integer age*/) {
        this.name = name;
        this.emailId = email;
        this.dob = dob;
        System.out.println("ttttttttttttt");
       // this.age = age;
    }

    public Student(String name, String email, LocalDate dob , Guardian guardian) {
        this.name = name;
        this.emailId = email;
        this.dob = dob;
        this.guardian = guardian;
        System.out.println("kkkkkkkkkkkkkkk");
        // this.age = age;
    }

    //cannot start without an all arg constructor , even when age is transient
    public Student(Long id , String name, String email , int age, LocalDate dob , Guardian guardian) {
        this.id = id;
        this.name = name;
        this.emailId = email;
        this.dob = dob;
        this.guardian = guardian;
        System.out.println("kkkkkkkkkkkkkkk");
        this.age = age;
    }

    //This overrides Lombok's getId()
    public Long getId() {
        System.out.println("inside getID()");
        return id;
    }

    public void setId(Long id) {
        System.out.println("inside setID()");
        this.id = id;
    }

    public String getName() {
        System.out.println("inside getName()");
        return name;
    }

    public void setName(String name) {
        System.out.println("inside setName()");
        this.name = name;
    }

    public String getEmailId() {
        System.out.println("inside getEmail()");
        return emailId;
    }

    public void setEmailId(String emailId) {
        System.out.println("inside setEmail()");
        this.emailId = emailId;
    }

    public LocalDate getDob() {
        System.out.println("inside getDob()");
        return dob;
    }

    public void setDob(LocalDate dob) {
        System.out.println("inside setDob()");
        this.dob = dob;
    }

    public Integer getAge() {
        System.out.println("inside getAge()");
        return Period.between(dob,LocalDate.now()).getYears();
    }

    public void setAge(Integer age) {
        System.out.println("inside setAge()");
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", emailId='" + emailId + '\'' +
                ", dob=" + dob +
                ", age=" + age +
                ", guardian=" + guardian +
                '}';
    }
}