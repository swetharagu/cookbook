package com.cookbook.app.dummy;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;

//@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@AttributeOverrides({
        @AttributeOverride(
                name = "name",
                column = @Column(name = "guardian_name")),
        @AttributeOverride(
                name = "emailId",
                column = @Column(name = "guardian_email")),
        @AttributeOverride(
                name = "mobileNumber",
                column = @Column(name = "guardian_mobile"))
})
public class Guardian {
    String name;
    String emailId;
    String mobileNumber;
}
