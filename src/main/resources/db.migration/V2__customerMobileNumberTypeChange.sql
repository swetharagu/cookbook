ALTER TABLE customer ADD CONSTRAINT uc_customer_mobile_number UNIQUE (mobile_number);

ALTER TABLE customer DROP COLUMN mobile_number;

ALTER TABLE customer ADD mobile_number VARCHAR(10) NOT NULL;


ALTER TABLE customer ADD CONSTRAINT uc_customer_mobile_number UNIQUE (mobile_number);